# Importações necessárias para o algoritmo
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


# Função principal na qual chamará todas as outras funções do algortitmo 
def fit(dataSet):

    # Chamando a função que lerá os dados e retornará os vetores necessários para o algoritmo
    X, Y = read_data(dataSet)

    # Chamando a função que normalizará os dados de entrada
    X, Y = normalization(X, Y)

    # Transformando o vetor de característica e o vetor de classe para array do numpy
    X = np.array(X)
    Y = np.array(Y)

    # Calculando a média de cada vetor dos dados
    X_ = np.mean(X)
    y_ = np.mean(Y)

    # Calculando o coeficiente linear do algoritmo pela fórmula:
    # SOMATÓRIO de ( xi - MÉDIA DO VETOR DE CARACTERÍSTICAS ) X ( yi - MÉDIA DO VETOR DA CLASSE ) / SOMATÓRIO de ( xi - MÉDIA DO VETOR DE CARACTERÍSTICAS ) ²
    w1 = np.sum((X - X_) * (Y - y_)) / np.sum((X - X_) ** 2)

    # Calculando o valor independente do algoritmo pela fórmula:
    # MÉDIA DO VETOR DA CLASSE - ( COEFICIENTE LINEAR x MÉDIA DO VETOR DE CARACTERÍSTICAS )
    w0 = y_ - ( w1 * X_ )

    # Declarando a lista que armazenará os valores preditos pelo modelo treinado
    y_predict = predict(w0, w1, X, Y)

    # Exibindo o método de desempenho MSE
    print(f"ERRO QUADRÁTICO MÉDIO: {MSE(Y, y_predict)}", end="\n")

    # Exibindo o gráfico do modelo
    graphic(X, Y, y_predict)


# Função que ler os dados do nosso algoritmo usando a biblioteca do pandas
def read_data(dataSet):

    # Primeiramente teremos que ler os dados usando o pandas
    # Usando o parametro "names", podemos informar o cabeçalho da tabela
    data = pd.read_csv(dataSet, names=["Atributo", "Classe"])

    # Retornando o vetor de atributos ( X ) e o vetor da classe ( Y )
    return list(data["Atributo"]), list(data["Classe"])


# Função que gera o gráfico da aplicação para análises
def graphic(X, Y, y_predict):

    # Definindo o tamanho da tela que terá o gráfico
    plt.figure(figsize=(8, 8))

    # Definindo o tipo do gráfico e qual seráo eixo X e Y dele
    plt.scatter(X, Y)

    # Exibindo a linha do modelo
    plt.plot(X, y_predict, 'r')

    # Exibindo o gráfico
    plt.show()


# Função que calcula a predição do modelo depois de treinado
def predict(w0, w1, X, Y):

    # Declarando a lista que armazenará os valores preditos pelo modelo
    y_predict = []

    # Para cada característica e classe dentro dos vetores de características e de classes 
    for xi, yi in zip(X, Y):

        # Calculando a predição do modelo
        y_ = w0 + w1 * xi

        # Adicionando a perdição do modelo dentro da lista de predições
        y_predict.append(y_)

    # Retorna a lista de predições do modelo
    return y_predict


# Função que calcula o MEAN SQUARE ERROR ( Erro quadrático médio )
def MSE(Y, y_):

    # Retornando o calculo do Erro quadrático médio pela fórmula:
    # SOMATÓRIO de ( yi - MÉDIA DO VETOR DA CLASSE ) ² / Total de elementos dentro do vetor de saída
    return np.sum((Y - y_) ** 2)/len(Y)


# Função que normalizará os dados de treino, removendo seus pontos fora da curva
def normalization(X:list, Y:list):

    # Fazendo uma cópia dos vetores de treino
    X_copy = X.copy()
    Y_copy = Y.copy()

    # Para cada index no intervalo de 0 ao tamanho do vetor
    for index in range(len(X)):

        # Se o yi for maior ou igual a 10 e xi for menor ou igual a 10 ou yi for menor ou igual a -10
        if Y[index] >= 10 and X[index] <= 10 or Y[index] <= -10:

            # Removendo os pontos dos vetores de cópia
            X_copy.remove(X[index])
            Y_copy.remove(Y[index])

    # Retornando os vetores de cópia
    return X_copy, Y_copy


if __name__ == '__main__':
    
    fit("segundo_trabalho/dados3.csv")
