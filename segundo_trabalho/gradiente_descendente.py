# Importações necessárias para o algoritmo
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


# Função que ler os dados do nosso algoritmo usando a biblioteca do pandas
def read_data(dataSet):

    # Primeiramente teremos que ler os dados usando o pandas
    # Usando o parametro "names", podemos informar o cabeçalho da tabela
    data = pd.read_csv(dataSet, names=["Atributo", "Classe"])

    # Retornando o vetor de atributos ( X ) e o vetor da classe ( Y )
    return list(data["Atributo"]), list(data["Classe"])


def calcular_gradiente_descendente(w0, w1, X, Y, alpha=0.001, epoch=20000):

    # Para cada época no intervalo de 0 a quantidade de épocas determinada
    for _ in range(epoch):

        # Calculando o erro do w0 anterior dado pela fórmula:
        # SOMATÓRIO DE ( VALOR PREDITO - VALOR REAL )
        w0Error = np.sum(predict(w0, w1, X) - Y)

        # Calculando o erro do w1 anterior dado pela fórmula:
        # SOMATÓRIO DE (( VALOR PREDITO - VALOR REAL ) x Xi)
        w1Error = np.sum((predict(w0, w1, X) - Y) * X)
        
        # Atualizando os pesos atuais do algoritmo
        w0 = w0 - alpha * (1 / len(X)) * w0Error
        w1 = w1 - alpha * (1 / len(X)) * w1Error
    
    return w0, w1


# Função que calcula o MEAN SQUARE ERROR ( Erro quadrático médio )
def MSE(Y, y_):

    # Retornando o calculo do Erro quadrático médio pela fórmula:
    # SOMATÓRIO de ( yi - MÉDIA DO VETOR DA CLASSE ) ²
    return np.sum((Y - y_) ** 2)/len(Y)


# Função que calcula a predição do modelo depois de treinado
def predict(w0, w1, x):

    # Retorna o valor predito pela fórmula:
    # VALOR DEPENDENTE + ( COEFICIENTE LINEAR x Xi )
    return w0 + ( w1 * x )


# Função que gera o gráfico da aplicação para análises
def graphic(X, Y, y_predict):

    # Definindo o tamanho da tela que terá o gráfico
    plt.figure(figsize=(18, 7))

    # Definindo o tipo do gráfico e qual seráo eixo X e Y
    plt.subplot(1, 2, 1)
    plt.scatter(X, Y)

    # Exibindo a linha do modelo
    plt.plot(X, predict(10, 1, X), 'b')

    # Exibindo a linha do modelo
    plt.subplot(1, 2, 2)
    plt.scatter(X, Y)
    plt.plot(X, y_predict, 'r')

    # Exibindo o gráfico
    plt.show()


# Função que normalizará os dados de treino, removendo seus pontos fora da curva
def normalization(X:list, Y:list):

    # Fazendo uma cópia dos vetores de treino
    X_copy = X.copy()
    Y_copy = Y.copy()

    # Para cada index no intervalo de 0 ao tamanho do vetor
    for index in range(len(X)):

        # Se o yi for maior ou igual a 10 e xi for menor ou igual a 10 ou yi for menor ou igual a -10
        if Y[index] >= 10 and X[index] <= 10 or Y[index] <= -10:

            # Removendo os pontos dos vetores de cópia
            X_copy.remove(X[index])
            Y_copy.remove(Y[index])

    # Retornando os vetores de cópia
    return X_copy, Y_copy


# Função principal na qual chamará todas as outras funções do algortitmo 
def fit(dataSet):

    # Chamando a função que lerá os dados e retornará os vetores necessários para o algoritmo
    X, Y = read_data(dataSet)

    # Chamando a função que normalizará os dados de entrada
    X, Y = normalization(X, Y)

    # Transformando o vetor de característica e o vetor de classe para array do numpy
    X = np.array(X)
    Y = np.array(Y)

    # Seprando dados de treino e dados de teste
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.3)

    # Exibindo o erro MSE antes do treino
    print(f"ERRO MSE ANTES DO TREINO: {MSE(Y_test, predict(10, 1, X_test))}")

    # Calculando o valor ideal para o coeficiente linear e angular do modelo
    w0, w1 = calcular_gradiente_descendente(1, 1, X_train, Y_train)

    # Exibindo o erro MSE depois do treino
    print(f"ERRO MSE DEPOIS DO TREINO: {MSE(Y_test, predict(w0, w1, X_test))}")

    # Exibindo o gráfico
    graphic(X, Y, predict(w0, w1, X))


if __name__ == '__main__':

    fit("segundo_trabalho/dados3.csv")
