# Importações necessárias para o algoritmo
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix


# Função que ler os dados do nosso algoritmo usando a biblioteca do pandas
def read_data(dataSet):

    # Primeiramente teremos que ler os dados usando o pandas
    # Usando o parametro "names", podemos informar o cabeçalho da tabela
    data = pd.read_csv(dataSet, names=["Atributo 1", "Atributo 2", "Classe"])

    # Declarando uma lista que receberá os valores das colunas formatados
    lista = []

    # Para cada index no intervalo de 0 ao tamanho do DataSet
    for i in range(0, len(data)):

        l = []

        # Adicionando na lista
        l.append(data[f"Atributo 1"].apply(lambda x: float(str(x).replace(".","")))[i])
        l.append(data[f"Atributo 2"].apply(lambda x: float(str(x).replace(".","")))[i])

        # Adicionando na lista principal
        lista.append(l)


    # Retornando o vetor de atributos ( X ) e o vetor da classe ( Y )
    return np.array(lista), np.array(list(data["Classe"]))


# Função que gera o gráfico da aplicação para análises
def graphic(X, Y, y_predict):

    # Definindo o tipo do gráfico e qual seráo eixo X e Y
    for label in y_predict:

        colors = ['r' if label == 0 else 'b' for label in y_predict]

    plt.scatter(X[:, 0], Y, color=colors)

    # Exibindo o gráfico
    plt.show()


# Função principal na qual chamará todas as outras funções do algortitmo 
def fit(dataSet):

    model = LogisticRegression()

    # Chamando a função que lerá os dados e retornará os vetores necessários para o algoritmo
    X, Y = read_data(dataSet)

    # Transformando o vetor de característica e o vetor de classe para array do numpy
    X = np.array(X)
    Y = np.array(Y)

    # Chamando a função que normalizará os dados de entrada
    # X = normalization(X)

    # Seprando dados de treino e dados de teste
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, stratify=Y)

    # Dando um fit no modelo
    model.fit(X_train, y_train)

    # Predizendo os valores
    y_pred = model.predict(X_test)

    # Calculando a acurácia
    accuracy = model.score(X_test, y_test)
    print('Acurácia:', accuracy)

    # Exibindo a matriz de confusão
    print(confusion_matrix(y_test, y_pred))

    # Exibindo o gráfico
    graphic(X_test, y_test, y_pred)


if __name__ == '__main__':

   fit('segundo_trabalho/dados2.csv')
