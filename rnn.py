# BR=ERT

class Vocab:

    def __init__(self):
        self.PAD = 0
        self.INICIO = 1
        self.FIM = 2
        self.palavraParaNumero = {}
        self.numeroParaPalavra = {}
    

        self.n_vocab = 3
    

    def adicionar_palavra(self, palavra):

        if palavra not in self.palavraParaNumero:
            self.palavraParaNumero[palavra] = self.n_vocab
            self.numeroParaPalavra[self.n_vocab] = palavra
            self.n_vocab += 1
    

    def getPalavra(self, numero):
        # print(self.numeroParaPalavra)
        return self.numeroParaPalavra[numero]
    

    def getNumero(self, palavra):
        return self.palavraParaNumero[palavra]


vocab = Vocab()
corpus = ["É muito bom essa manga", "É show essa manga da sua camisa"]

for frase in corpus:

    for palavra in frase.split(' '):
        vocab.adicionar_palavra(palavra)


for i in range(3, vocab.n_vocab):

    print(vocab.getPalavra(i))