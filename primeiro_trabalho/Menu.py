from Valdiacao import Validacao
import sys
from KNN import *

class Menu:

    opcoes = """
    [ 1 ] - Ler dados 1
    [ 2 ] - Ler dados 2
    [ 3 ] - Ler dados Abalone
    [ 4 ] - Sair do algoritmo
    """


    @staticmethod
    def _menu_principal():

        while True:

            print(Menu.opcoes)

            opcao_usuario = Validacao.ValidacaoNumeroInteiro("Digite sua opção: ")

            if opcao_usuario == 1:

                X, Y = ler_dados("primeiro_trabalho/dados1.csv", 2, 3)

                fix(X, Y, 7)

            elif opcao_usuario == 2:

                X, Y = ler_dados("primeiro_trabalho/dados2.csv", 2, 3)

                fix(X, Y, 7)

            elif opcao_usuario == 3:

                X, Y = ler_dados("primeiro_trabalho/abalone.csv", 8, 9, True)
                
                fix(X, Y, 183, True)

            elif opcao_usuario == 4:

                print("Obrigado por testar!!!")
                sys.exit()
            
            else:

                print("Opção inválida!!!")


