# Importações necessárias para o algoritmo funcionar 
import pandas as pd
from sklearn import preprocessing
import numpy as np
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import ConfusionMatrixDisplay


# Criando função que terá como parâmetro o DataSet fornecedido, quantidade de caracteres do DataSet e o número da coluna da Classe do DataSet
# Essa função lerá o DataSet fornecido para o algoritmo 
def ler_dados(dataset, quantidade_caracteristicas, numero_coluna_classe, abalone=False):

    # Listar os nomes das colunas de forma dinâmica
    nome_coluna = []

    # Para cada index no intervalo de 0 a quantidade de atributos do DataSet fornecido
    for i in range(quantidade_caracteristicas):

        # Adicionar a lista a String "Atributo (index + 1)" 
        nome_coluna.append(f"Atributo {i + 1}")
    
    # Adicionando a lista no index da classe o nome "Classe"
    nome_coluna.insert(numero_coluna_classe-1, "Classe")

    # Primeiramente teremos que ler os dados usando o pandas
    # Usando o parametro "names", podemos informar o cabeçalho da tabela
    data = pd.read_csv(dataset, names=nome_coluna)

    # Instânciando um LabelEncoder
    labelEncoder = preprocessing.LabelEncoder()
    labelEncoder.fit(data["Classe"])

    # Transformando os rótulos em apenas números inteiros para uma melhor performance do algoritmo
    data["Classe"] = labelEncoder.transform(data["Classe"])

    if abalone:

        labelEncoder.fit(data["Atributo 1"])

        # Transformando os rótulos em apenas números inteiros para uma melhor performance do algoritmo
        data["Atributo 1"] = labelEncoder.transform(data["Atributo 1"])

    # Pegando os atributos e os rótulos do DataSet
    if not abalone:

        X = pegar_atributos_dataSet(data, quantidade_caracteristicas)

    else:

        X = pegar_abalone(data, quantidade_caracteristicas)

    Y = pegar_rotulos_dataSet(data)

    # Retornando os atributos e os rótulos
    return X, Y



def pegar_abalone(dataSet, quantidade_caracteristicas):

    X = []

    for i in range(len(dataSet)):

            # Declarando uma lista que receberá os valores das colunas formatados
        lista = []

        # Para cada index no intervalo de 0 a quantidade de caracteres
        for j in range(quantidade_caracteristicas):

            data = dataSet[f"Atributo {j + 1}"][i]

            if "." in str(data):

                data = str(data).replace(".", "")

            lista.append(float(data))
        
        # Adicionando no vetor de características
        X.append(lista)

    return X


# Função que pega somente a coluna dos atributos dentro do Dataset, para isso precisa-se informar o DataSet e a quantidade de colunas
def pegar_atributos_dataSet(dataSet, quantidade):

    # Declarando o vetor de características
    X = []

    # Para cada index no intervalo de 0 ao tamanho do DataSet
    for i in range(len(dataSet)):

        # Declarando uma lista que receberá os valores das colunas formatados
        lista = []

        # Para cada index no intervalo de 0 a quantidade de caracteres
        for j in range(quantidade):

            # Adicionando na lista
            lista.append(dataSet[f"Atributo {j + 1}"].apply(lambda x: float(str(x).replace(".","")))[i])

        # Adicionando no vetor de características
        X.append(lista)
    
    # Retornando o vetor de caracteristicas
    return X


def pegar_rotulos_dataSet(dataSet):

    Y = []

    for i in range(len(dataSet["Classe"])):

        Y.append(dataSet["Classe"][i])

    return Y



# Função que calculará a distância euclidiana entre 2 pontos num plano cartesiano
# Recebe como parâmetro dois vetores, que simbolizam as coordenadas do ponto no plano cartesiano
def distancia_euclidiana(ponto1, ponto2):

    # Incializando a variável que guardará a distância dos pontos
    distancia = 0

    # Para cada index no intervalo de 0 ao tamanho do vetor do primeiro ponto
    for i in range(len(ponto1)):

        # A variável distância passa a ser a distância mais a subtração das coordenadas dos pontos elevado ao quadrada
        distancia += (ponto1[i] - ponto2[i]) ** 2
    
    # Retornando a raiz quadrada da variável distância 
    return distancia ** 0.5


# Função que retorna a lista de vizinhos próximos de acordo com a quantidade K
def vizinhos_proximos(novo_ponto, treino, k):

    # Declarando uma lista vazia que armazenará as distâncias de cada ponto
    distancias = []

    # Declarando uma lista vazia que receberá o INDEX de cada vizinho da lista
    vizinhos = []

    # Para cada coordenada dentro do teste
    for index, treino_ponto in enumerate(treino):

        # Pegando a distância euclidiana com ajuda da função criada anteriormente
        distancia = distancia_euclidiana(novo_ponto, treino_ponto)

        # Adicionando a lista de distâncias, o INDEX e a DISTÂNCIA de cada coordenada do treino
        distancias.append((index, distancia))
    
    # Organizando a lista de distâncias de acordo com o segundo valor ( Distância )
    distancias = sorted(distancias, key=lambda x: x[1])

  
    # Para cada INDEX no intervalo de 0 a quantidade K
    for i in range(k):

        # Adicionando a lista de vizinhos o INDEX referente a distância de acordo com o INDEX
        vizinhos.append(distancias[i][0])

    # Retornando a lista de INDEX dos vizinhos próximos
    return vizinhos


# Função que classifica o novo ponto de coordenada usando o modelo KNN
def classificar(novo_ponto, X_treino, Y_treino, k):

    # Pegando a lista de vizinhos próximos
    vizinhos = vizinhos_proximos(novo_ponto, X_treino, k)

    # Declarando uma lista vázia que armazenrá as coordenadas do vetor de rótulo de teste
    vizinhos_y = []

    # Para cada vizinho dentro da lista de vizinhos próximos
    for i in vizinhos:
        
        # Adicionando cada elemento dentro da lista de vizinhos de acordo com o index
        vizinhos_y.append(Y_treino[i])

    # Calculando a predição de acordo com a quantidade de cada rótulo, quem tiver maior quantidade acaba sendo selecionado na função
    predicao = max(set(vizinhos_y), key=vizinhos_y.count)

    # Retornando a predição, ou seja, o rótulo do novo ponto
    return predicao


# Função que normalizará os dados para que eles atuem na mesma escala
def normalização_maxima(X_treino, abalone=False):

    # Tirando o mínimo de cada coluna
    min0 = min(X_treino[:, 0])
    min1 = min(X_treino[:, 1])

    if abalone:
        min2 = min(X_treino[:, 2])
        min3 = min(X_treino[:, 3])
        min4 = min(X_treino[:, 4])
        min5 = min(X_treino[:, 5])
        min6 = min(X_treino[:, 6])
        min7 = min(X_treino[:, 7])

    # Tirando o máximo de cada coluna
    max0 = max(X_treino[:, 0])
    max1 = max(X_treino[:, 1])

    if abalone:
        max2 = max(X_treino[:, 2])
        max3 = max(X_treino[:, 3])
        max4 = max(X_treino[:, 4])
        max5 = max(X_treino[:, 5])
        max6 = max(X_treino[:, 6])
        max7 = max(X_treino[:, 7])

    if abalone:

        # Para cada index no intervalo de 0 ao tamanho do vetor de características
        for i in range(X_treino.shape[0]):

            # Efetuando o cálculo de normalização e atribuindo a cada coluna
            X_treino[i, 0] = round((X_treino[i, 0] - min0)/(max0 - min0), 2)
            X_treino[i, 1] = round((X_treino[i, 1] - min1)/(max1 - min1), 2)
            X_treino[i, 2] = round((X_treino[i, 0] - min2)/(max2 - min2), 2)
            X_treino[i, 3] = round((X_treino[i, 1] - min3)/(max3 - min3), 2)
            X_treino[i, 4] = round((X_treino[i, 0] - min4)/(max4 - min4), 2)
            X_treino[i, 5] = round((X_treino[i, 1] - min5)/(max5 - min5), 2)
            X_treino[i, 6] = round((X_treino[i, 0] - min6)/(max6 - min6), 2)
            X_treino[i, 7] = round((X_treino[i, 1] - min7)/(max7 - min7), 2)

    else:

        # Para cada index no intervalo de 0 ao tamanho do vetor de características
        for i in range(X_treino.shape[0]):

            # Efetuando o cálculo de normalização e atribuindo a cada coluna
            X_treino[i, 0] = round((X_treino[i, 0] - min0)/(max0 - min0), 2)
            X_treino[i, 1] = (X_treino[i, 1] - min1)/(max1 - min1)
        
    # Retornando o valor de características normalizado
    return X_treino


# Função principal que chamará todas as outras
def fix(X, Y, k, abalone=False):

    # Transformando o vetor de caracteristicas em um vetor normalizado
    X = normalização_maxima(np.array(X), abalone)

    # Seprando dados de treino e dados de teste
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.4)

    if not abalone:

        # Exibindo o gráfico
        _grafico_scatter(X_train, y_train)

        # Exibindo a matriz de confusão
        print("MATRIZ DE CONFUSÃO: ")
        matriz = matriz_confusao(X_train, X_test, y_train, y_test, k)
        # Para cada index no intervalo de 0 e o tamanho do vetor de caracteristicas de tste

        # Lista de valores preditos
        y_pred = []

        for index in range(len(X_test)):

            # Pegando o rótulo saida do ponto novo
            y_ = classificar(X_test[index], X_train, y_train, k)
            y_pred.append(y_)

        # Fazer odisplay da matriz de confusão
        cm_display = ConfusionMatrixDisplay(matriz)

        # Plotando os dados
        cm_display.plot()

        # Mostrando os dados
        plt.show()

        print(matriz, end="\n\n")

        # Exibindo a precisão
        print("PRECISÃO: ", end="")
        print(precisao(matriz), end="\n\n")

        # Exibindo o recall
        print("RECALL: ", end="")
        print(recall(matriz), end="\n\n")

        # Exibindo o F1 Score
        print("F1 Score: ", end="")
        print(f1Score(matriz), end="\n\n")

    # Exibindo a acurácia
    print("ACURÁCIA: ", end="")
    print(acuracia(X_train, X_test, y_train, y_test, k), end="\n\n")

    

# Função que calcula o gráfico Scatter dos dados de treino
def _grafico_scatter(X_train, Y_train):

    # Separando cada coordenada
    rotulo_0 = []
    rotulo_1 = []

    for i in range(len(Y_train)):

        if Y_train[i] == 0:

            rotulo_0.append(X_train[i, :])
        
        else:

            rotulo_1.append(X_train[i, :])

    rotulo_0 = np.array(rotulo_0)
    rotulo_1 = np.array(rotulo_1)

    x_0 = rotulo_0[:, 0]
    y_0 = rotulo_0[:, 1]

    x_1 = rotulo_1[:, 0]
    y_1 = rotulo_1[:, 1]

    # Construindo o gráfico
    plt.scatter(x_0, y_0, color = 'blue')
    # Construindo o gráfico
    plt.scatter(x_1, y_1, color = 'red')

    # Exibindo o gráfico
    plt.show()


def precisao(matriz_confusao):

    TP = matriz_confusao[0, 0]
    FP = matriz_confusao[1, 0]

    precisao = TP / (TP + FP)

    return precisao


def recall(matriz_confusao):

    TP = matriz_confusao[0, 0]
    FN = matriz_confusao[0, 1]

    recall = TP / (TP + FN)

    return recall


def f1Score(matriz_confusao):

    recall_response = recall(matriz_confusao)
    precisao_response = precisao(matriz_confusao)

    f1Score = 2 * ((precisao_response * recall_response) / (precisao_response + recall_response))

    return f1Score


# Função que calcula a acuracia
def acuracia(X_train, X_test, y_train, y_test, k):

    # Declarando a variável de acertos
    acertos = 0

    # Para cada index no intervalo de 0 e o tamanho do vetor de caracteristicas de tste
    for index in range(len(X_test)):

        # Pegando o rótulo saida do ponto novo
        y_ = classificar(X_test[index], X_train, y_train, k)

        # Se o rótulo saida for igual ao rótulo real
        if y_ == y_test[index]:

            # Soma-se mais 1 a variável acertos
            acertos += 1
        
    # Retornando acertos dividido pelo total de itens no vetor de caracteristicas
    return acertos/len(X_test)

                              
# Função que calcula a matriz de confusão para cada teste
def matriz_confusao(X_train, X_test, y_train, y_test, k):

    # Primeira linha = Real True
    # Segunda linha = Real False

    # Primeira coluna = Predição True
    # Segunda coluna = Predição False

    quantidade_total_0_y = 0
    quantidade_total_1_y = 0

    quantidade_total_acertos_0_y = 0
    quantidade_total_acertos_1_y = 0
    quantidade_total_erros_0_y = 0
    quantidade_total_erros_1_y = 0

    # Declarando uma matriz
    matriz = np.array([[0, 0], [0, 0]])

    # Para cada rótulo dentro do vetor de rótulos
    for y in y_test:

        # Se o rótulo for igual a 0
        if y == 0:

            # Somando ao total de 0
            quantidade_total_0_y += 1   
        
        # Se o rótulo for igual a 1
        else:

            # Somando ao total de 1
            quantidade_total_1_y += 1

    # Para cada index no intervalo de 0 a quantidade de caracteres de teste
    for index in range(len(X_test)):

        # Classificando cada ponto do vetor de caracteristicas de teste
        y_ = classificar(X_test[index], X_train, y_train, k)

        # Se o rótulo saida for igual ao rótulo real
        if y_ == y_test[index]:

            # Se o rótulo for igual a 0
            if y_ == 0:

                quantidade_total_acertos_0_y += 1
            
            # Se o rótulo for igual a 1
            else:

                quantidade_total_acertos_1_y += 1

        # Se o rótulo saida não for igual ao rótulo real 
        else:

            # Se o rótulo for igual a 0
            if y_ == 0:

                quantidade_total_erros_0_y += 1
            
            # Se o rótulo for igual a 1
            else:

                quantidade_total_erros_1_y += 1

    # Atribuindo os valores a cada ponto da matriz
    matriz[1, 1] = quantidade_total_acertos_0_y
    matriz[0, 0] = quantidade_total_acertos_1_y
    matriz[0, 1] = quantidade_total_erros_1_y
    matriz[1, 0] = quantidade_total_erros_0_y

    # Retornando a matriz
    return matriz


def ler_abalone(dataSet, quantidade_caracteristicas, numero_coluna_classe):

    # Listar os nomes das colunas de forma dinâmica
    nome_coluna = []

    # Para cada index no intervalo de 0 a quantidade de atributos do DataSet fornecido
    for i in range(quantidade_caracteristicas):

        # Adicionar a lista a String "Atributo (index + 1)" 
        nome_coluna.append(f"Atributo {i + 1}")
    
    # Adicionando a lista no index da classe o nome "Classe"
    nome_coluna.insert(numero_coluna_classe-1, "Classe")

    data = pd.read_csv(dataSet, names=nome_coluna)
    labelEncode = preprocessing.LabelEncoder()
    labelEncode.fit(data["Atributo 1"])

    # Transformando os rótulos em apenas números inteiros para uma melhor performance do algoritmo
    data["Atributo 1"] = labelEncode.transform(data["Atributo 1"])
    
    print(data)
    