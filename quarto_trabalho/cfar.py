import torch
import torch.nn as nn
import torch.nn.functional as F

import torch.utils.data

from torchvision import datasets
import torchvision.transforms as T

import matplotlib.pyplot as plt

flat_train_data = datasets.CIFAR10(root = 'data', train=True, transform= T.Compose([
    T.ToTensor(),
    T.Lambda(lambda x: x.view(3, 32, 32))
]), download=True)
flat_test_data = datasets.CIFAR10(root = 'data', train=False, transform= T.Compose([
    T.ToTensor(),
    T.Lambda(lambda x: x.view(3, 32, 32))
]), download=True)

# print(flat_train_data.data.shape)
# plt.imshow(flat_train_data.data[100], cmap='gray')
# plt.title(flat_train_data[100][1])
# plt.show()

# figure = plt.figure(figsize=(10,8))
# cols, rowns = 5, 5

BATCH_SIZE = 100
train_loader = torch.utils.data.DataLoader(flat_train_data, batch_size=BATCH_SIZE, shuffle=True)
test_loader = torch.utils.data.DataLoader(flat_test_data, batch_size=BATCH_SIZE, shuffle=True)

# for i in range(1, cols*rowns + 1):

#     sample_idx = torch.randint(len(flat_train_data), size=(1,)).item()
#     img, label = flat_train_data[sample_idx]
#     figure.add_subplot(rowns, cols, i)
#     plt.title(label)
#     plt.axis("off")
#     plt.imshow(img.squeeze(), cmap='gray')

# plt.show()


class CNN(nn.Module):

    def __init__(self):

        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, kernel_size=5, stride=1)
        self.conv2 = nn.Conv2d(6, 16, kernel_size=5, stride=1)
        self.fc1 = nn.Linear(16 * 5 * 5, 256)
        self.fc2 = nn.Linear(256, 10)

    def forward(self, X):

        X = self.conv1(X)
        X = F.relu(X)
        X = F.max_pool2d(X, 2)
        X = self.conv2(X)
        X = F.max_pool2d(X, 2)
        X = F.relu(X)
        X = X.view(-1 , 16 * 5 * 5)
        X = self.fc1(X)
        X = F.relu(X)
        X = self.fc2(X)
        X = F.log_softmax(X, dim=1)
        return X


cnn = CNN()

# class cnn(nn.Module):
#     def __init__(self):
#         super(cnn, self).__init__()
#         self.fc1 = nn.Linear(784, 10)

#     def forward(self, x):
#         x = self.fc1(x)
#         return F.log_softmax(x, dim=1)


# Criar a instância do modelo da cnn
# cnn = cnn()


# Definir a função de perda (loss) e o otimizador
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(cnn.parameters(), lr=0.01)

# Exemplo de treinamento da cnn usando dados fictícios
# Aqui você precisará fornecer seus próprios dados de treinamento
# e realizar o pré-processamento adequado, como normalização e divisão em lotes.

# Treinar a cnn
num_epochs = 5
for epoch in range(num_epochs):

    model = cnn.train()
    acertos = 0

    for batch_idx, (X_branch, Y_branch) in enumerate(train_loader):
        optimizer.zero_grad()
        outputs = cnn(X_branch)

        print(len(outputs))

        loss = criterion(outputs, Y_branch)
        loss.backward()
        optimizer.step()

        preditos = torch.max(outputs.data, 1)[1]
        acertos += (preditos == Y_branch).sum()

        if batch_idx % 50 == 0:
            print(f"{round(float(acertos) / float(BATCH_SIZE*(batch_idx+1))*100, 2)} %")

            # print("Epoch: {} [{}/{} ({:.0f})%]\t {:.6f}\t")



#     # Forward pass
    
    

#     # Backward pass e otimização
    
    

#     # train_running_loss += loss.detach().item()
#     # model.eval()
#     # print('Epoch: %d | Loss: %.4f' %(epoch, train_running_loss) )