import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.datasets import ImageFolder

import torch.utils.data

from torchvision import datasets
import torchvision.transforms as T

import matplotlib.pyplot as plt

# Diretório contendo as imagens de treinamento
train_dir = "animais2/animals"


# Definir os parâmetros do pré-processamento das imagens
# Definir transformações para as imagens

# Carregar as imagens da pasta
dataset = ImageFolder(train_dir, transform=T.Compose([
    T.Resize((750, 750)),  # Redimensionar todas as imagens para o mesmo tamanho
    T.ToTensor()  # Converter as imagens em tensores
]))


BATCH_SIZE = 100
train_loader = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=True)
 
# print(len(train_loader))


class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, kernel_size=5, stride=1)
        self.conv2 = nn.Conv2d(6, 16, kernel_size=5, stride=1)
        self.conv3 = nn.Conv2d(16, 16, kernel_size=4, stride=1)
        self.fc1 = nn.Linear(16 * 25, 256)
        self.fc2 = nn.Linear(256, 10)

    def forward(self, X):
        X = self.conv1(X)
        X = F.relu(X)
        X = F.max_pool2d(X, 6)
        X = self.conv2(X)
        X = F.max_pool2d(X, 4)
        X = F.relu(X)
        X = self.conv3(X)
        X = F.max_pool2d(X, 5)
        X = F.relu(X)
        X = X.view(-1, 16 * 25)
        X = self.fc1(X)
        X = F.relu(X)
        X = self.fc2(X)
        X = F.softmax(X, dim=1)
        return X



cnn = CNN()

# class cnn(nn.Module):
#     def __init__(self):
#         super(cnn, self).__init__()
#         self.fc1 = nn.Linear(784, 10)

#     def forward(self, x):
#         x = self.fc1(x)
#         return F.log_softmax(x, dim=1)


# Criar a instância do modelo da cnn
# cnn = cnn()


# Definir a função de perda (loss) e o otimizador
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(cnn.parameters(), lr=0.01)

# Exemplo de treinamento da cnn usando dados fictícios
# Aqui você precisará fornecer seus próprios dados de treinamento
# e realizar o pré-processamento adequado, como normalização e divisão em lotes.

# Treinar a cnn
# for images, labels in train_loader:
#     # plt.imshow(images)
#     print(images.size())
#     # plt.title(labels)
#     # plt.show()
num_epochs = 1
for epoch in range(num_epochs):

    model = cnn.train()
    acertos = 0

    for batch_idx, (X_branch, Y_branch) in enumerate(train_loader):
        # print(len(Y_branch))
        optimizer.zero_grad()
        outputs = cnn(X_branch)
        # print(len(outputs))
        # print(Y_branch)

        loss = criterion(outputs, Y_branch)
        # print("PASOUUUUUUUUUUUUUUUUUUUUUU")
        loss.backward()
        optimizer.step()

        preditos = torch.max(outputs.data, 1)[1]
        acertos += (preditos == Y_branch).sum()

        if batch_idx % 2 == 0:
            print(f"{round(float(acertos) / float(BATCH_SIZE*(batch_idx+1))*100, 2)} %")

            # print("Epoch: {} [{}/{} ({:.0f})%]\t {:.6f}\t")



#     # Forward pass
    
    

#     # Backward pass e otimização
    
    

    # train_running_loss += loss.detach().item()
    model.eval()
#     # print('Epoch: %d | Loss: %.4f' %(epoch, train_running_loss) )