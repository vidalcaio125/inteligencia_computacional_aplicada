import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.datasets import ImageFolder

import torch.utils.data

from torchvision import datasets
import torchvision.transforms as T

import matplotlib.pyplot as plt

# Diretório contendo as imagens de treinamento
train_dir = "animais2/animals"


# Definir os parâmetros do pré-processamento das imagens
# Definir transformações para as imagens

# Carregar as imagens da pasta
dataset = ImageFolder(train_dir, transform=T.Compose([
    T.Resize((224, 224)),
    T.ToTensor(),
    T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))  # Normalização das imagens
]))


BATCH_SIZE = 100
train_loader = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=True)
 
# print(len(train_loader))


class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(3, 32, kernel_size=5, stride=1)
        self.conv2 = nn.Conv2d(32, 16, kernel_size=5, stride=1)
        self.fc1 = nn.Linear(16 * 5 * 5, 500)
        self.fc2 = nn.Linear(500, 3)  # Alterada para ter 3 unidades de saída (número de classes)

    def forward(self, X):
        X = self.conv1(X)
        X = F.relu(X)
        X = F.max_pool2d(X, 4)
        X = self.conv2(X)
        X = F.max_pool2d(X, 9)
        X = F.relu(X)
        X = X.view(-1, 16 * 5 * 5)
        X = self.fc1(X)
        X = F.relu(X)
        X = self.fc2(X)
        X = F.softmax(X, dim=1)
        return X



cnn = CNN()

# class cnn(nn.Module):
#     def __init__(self):
#         super(cnn, self).__init__()
#         self.fc1 = nn.Linear(784, 10)

#     def forward(self, x):
#         x = self.fc1(x)
#         return F.log_softmax(x, dim=1)


# Criar a instância do modelo da cnn
# cnn = cnn()


# Definir a função de perda (loss) e o otimizador
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(cnn.parameters(), lr=0.01)


num_epochs = 10
for epoch in range(num_epochs):

    model = cnn.train()
    acertos = 0

    for batch_idx, (X_branch, Y_branch) in enumerate(train_loader):

        optimizer.zero_grad()
        outputs = cnn(X_branch)

        loss = criterion(outputs, Y_branch)
        # print("PASOUUUUUUUUUUUUUUUUUUUUUU")
        loss.backward()
        optimizer.step()

        preditos = torch.max(outputs.data, 1)[1]
        acertos += (preditos == Y_branch).sum()

        print(f"{round(float(acertos) / float(BATCH_SIZE*(batch_idx+1))*100, 2)} %")

            # print("Epoch: {} [{}/{} ({:.0f})%]\t {:.6f}\t")



#     # Forward pass
    
    

#     # Backward pass e otimização
    
    

    # train_running_loss += loss.detach().item()
    model.eval()
#     # print('Epoch: %d | Loss: %.4f' %(epoch, train_running_loss) )