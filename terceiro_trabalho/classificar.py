# Importações necessárias para o algoritmo
import torch
import torch.nn as nn
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


class Funcionalidade:

    @staticmethod
    # Função que calcula o gráfico Scatter dos dados de treino
    def _grafico_scatter(X):

        class_0 = X[y_pred == 0]
        class_1 = X[y_pred == 1]

        plt.scatter(class_0[:, 0], class_0[:, 1], c='red', label='Classe 0')
        plt.scatter(class_1[:, 0], class_1[:, 1], c='blue', label='Classe 1')

        # Exibindo o gráfico
        plt.show()
    
    # Criando função que terá como parâmetro o DataSet fornecedido, quantidade de caracteres do DataSet e o número da coluna da label do DataSet
    # Essa função lerá o DataSet fornecido para o algoritmo 
    @staticmethod
    def read_data(dataset, quantidade_caracteristicas, numero_coluna_label):

        # Listar os nomes das colunas de forma dinâmica
        nome_coluna = []

        # Para cada index no intervalo de 0 a quantidade de atributos do DataSet fornecido
        for i in range(quantidade_caracteristicas):

            # Adicionar a lista a String "Atributo (index + 1)" 
            nome_coluna.append(f"Atributo {i + 1}")
        
        # Adicionando a lista no index da label o nome "Label"
        nome_coluna.insert(numero_coluna_label - 1, "Label")

        # Primeiramente teremos que ler os dados usando o pandas
        # Usando o parametro "names", podemos informar o cabeçalho da tabela
        data = pd.read_csv(dataset, names=nome_coluna)

        print(data)

        # Pegando os atributos e os rótulos do DataSet
        X = Funcionalidade.pegar_atributos_dataSet(data, quantidade_caracteristicas)

        Y = Funcionalidade.pegar_rotulos_dataSet(data)

        # Retornando os atributos e os rótulos
        return X, Y


    # Função que pega somente a coluna dos atributos dentro do Dataset, para isso precisa-se informar o DataSet e a quantidade de colunas
    @staticmethod
    def pegar_atributos_dataSet(dataSet, quantidade):

        # Declarando o vetor de características
        X = []

        # Para cada index no intervalo de 0 ao tamanho do DataSet
        for i in range(len(dataSet)):

            # Declarando uma lista que receberá os valores das colunas formatados
            lista = []

            # Para cada index no intervalo de 0 a quantidade de caracteres
            for j in range(quantidade):

                # Adicionando na lista
                lista.append(dataSet[f"Atributo {j + 1}"].apply(lambda x: float(str(x).replace(".","")))[i])

            # Adicionando no vetor de características
            X.append(lista)
        
        # Retornando o vetor de caracteristicas
        return X

    @staticmethod
    def pegar_rotulos_dataSet(dataSet):

        Y = []

        for i in range(len(dataSet["Label"])):

            Y.append(dataSet["Label"][i])

        return Y


    # Função que normalizará os dados para que eles atuem na mesma escala
    @staticmethod
    def normalização_maxima(X_treino):

        # Tirando o mínimo de cada coluna
        min0 = min(X_treino[:, 0])
        min1 = min(X_treino[:, 1])

        # Tirando o máximo de cada coluna
        max0 = max(X_treino[:, 0])
        max1 = max(X_treino[:, 1])

        # Para cada index no intervalo de 0 ao tamanho do vetor de características
        for i in range(X_treino.shape[0]):

            # Efetuando o cálculo de normalização e atribuindo a cada coluna
            X_treino[i, 0] = round((X_treino[i, 0] - min0)/(max0 - min0), 2)
            X_treino[i, 1] = (X_treino[i, 1] - min1)/(max1 - min1)
            
        # Retornando o valor de características normalizado
        return X_treino


X, Y = Funcionalidade.read_data("terceiro_trabalho/dados2.csv", 2, 3)

# Normalizando os dados de características
X = Funcionalidade.normalização_maxima(np.array(X))

# Dividir os dados em conjuntos de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, stratify=Y)

# # Vetor de características para o modelo
X_train = torch.tensor(X_train, dtype=torch.float32)
y_train = torch.tensor(y_train, dtype=torch.float32)

X_test = torch.tensor(X_test, dtype=torch.float32)
y_test = torch.tensor(y_test, dtype=torch.float32)


# Definir a arquitetura da MLP
class MLP(nn.Module):
    def __init__(self):
        super(MLP, self).__init__() # Camada de entrada com 2 neurônios e 10 neurônios na camada oculta
        self.fc1 = nn.Linear(2, 10)  # Camada de entrada com 2 neurônios e 10 neurônios na camada oculta
        self.fc2 = nn.Linear(10, 1)  # Camada oculta com 10 neurônios e 1 neurônio na camada de saída
        self.activation = nn.ReLU()  # Função de ativação ReLU

    def forward(self, x):
        x = self.fc1(x)
        x = self.activation(x)
        x = self.fc2(x)
        x = torch.sigmoid(x)  # Função de ativação sigmoid na camada de saída para classificação binária
        return x

# Criar a instância do modelo da MLP
mlp = MLP()


# Definir a função de perda (loss) e o otimizador
criterion = nn.BCELoss()
optimizer = torch.optim.SGD(mlp.parameters(), lr=0.1)

# Exemplo de treinamento da MLP usando dados fictícios
# Aqui você precisará fornecer seus próprios dados de treinamento
# e realizar o pré-processamento adequado, como normalização e divisão em lotes.

# Treinar a MLP
num_epochs = 500
for epoch in range(num_epochs):

    model = mlp.train()
    train_running_loss = 0.0

    # Forward pass
    outputs = mlp(X_train)
    loss = criterion(outputs, y_train.unsqueeze(1))

    # Backward pass e otimização
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    train_running_loss += loss.detach().item()
    model.eval()
    print('Epoch: %d | Loss: %.4f' %(epoch, train_running_loss) )

# Exemplo de teste da MLP usando dados fictícios
# Fazer previsões nos dados de teste

with torch.no_grad():

    y_pred = mlp(X_test)
    y_pred = torch.round(y_pred).squeeze()


Funcionalidade._grafico_scatter(X_test)

# Calcular a acurácia das previsões
accuracy = accuracy_score(y_test, y_pred)
print("Acurácia: {:.2f}%".format(accuracy * 100))