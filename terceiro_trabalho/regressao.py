# Importações necessárias para o algoritmo
import torch
import torch.nn as nn
import numpy as np
import time
import matplotlib.pyplot as plt
import pandas as pd
from sklearn import preprocessing
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


class Funcionalidade:

    @staticmethod
    # Função que calcula o gráfico Scatter dos dados de treino
    # Função que gera o gráfico da aplicação para análises
    def graphic(X, Y, y_predict):


        plt.scatter(X, Y)
        plt.plot(X, y_predict, 'r')

        # Exibindo o gráfico
        plt.show()
    
    # Criando função que terá como parâmetro o DataSet fornecedido, quantidade de caracteres do DataSet e o número da coluna da label do DataSet
    # Essa função lerá o DataSet fornecido para o algoritmo 
    @staticmethod
    def read_data(dataset):

        # Primeiramente teremos que ler os dados usando o pandas
        # Usando o parametro "names", podemos informar o cabeçalho da tabela
        data = pd.read_csv(dataset, names=["Atributo", "Saida"])

        # Retornando o vetor de atributos ( X ) e o vetor da Saida ( Y )
        return list(data["Atributo"]), list(data["Saida"])


    # Função que normalizará os dados para que eles atuem na mesma escala
    # Função que normalizará os dados de treino, removendo seus pontos fora da curva
    @staticmethod
    def normalization(X:list, Y:list):

        # Fazendo uma cópia dos vetores de treino
        X_copy = X.copy()
        Y_copy = Y.copy()

        # Para cada index no intervalo de 0 ao tamanho do vetor
        for index in range(len(X)):

            # Se o yi for maior ou igual a 10 e xi for menor ou igual a 10 ou yi for menor ou igual a -10
            if Y[index] >= 10 and X[index] <= 10 or Y[index] <= -10:

                # Removendo os pontos dos vetores de cópia
                X_copy.remove(X[index])
                Y_copy.remove(Y[index])

        # Retornando os vetores de cópia
        return X_copy, Y_copy


X, Y = Funcionalidade.read_data("terceiro_trabalho/dados3.csv")

X, Y = Funcionalidade.normalization(X, Y)

# Dividir os dados em conjuntos de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2)


# # Vetor de características para o modelo
X_train = torch.tensor(X_train, dtype=torch.float32).reshape(-1, 1)
y_train = torch.tensor(y_train, dtype=torch.float32)

X_test = torch.tensor(X_test, dtype=torch.float32).reshape(-1, 1)
y_test = torch.tensor(y_test, dtype=torch.float32)


# Definir a arquitetura da MLP
class MLPRegression(nn.Module):
    def __init__(self):
        super(MLPRegression, self).__init__() # Camada de entrada com 1 neurônios e 5 neurônios na camada oculta
        self.camada1 = nn.Linear(1, 10)  # Camada de entrada com 1 neurônio e 2 neurônios na camada oculta
        self.camadaSaida = nn.Linear(10, 1)  # Camada oculta com 10 neurônios e 1 neurônio na camada de saída

    def forward(self, x):
        x = torch.relu(self.camada1(x))  # Função de ativação ReLU na camada oculta
        x = torch.relu(self.camadaSaida(x)) # Saída da rede neural
        return x

# Criar a instância do modelo da MLP
model = MLPRegression()


# Definir a função de perda (loss) e o otimizador
criterion = nn.MSELoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)


# Treinar a MLP
num_epochs = 1000
for epoch in range(num_epochs):
    model.train()
    
    ## forward
    output = model(X_train)
    loss = criterion(output, y_train)
    optimizer.zero_grad()

    ## backward + update model params 
    loss.backward()
    optimizer.step()


    model.eval()
    

# Exemplo de teste da MLP usando dados fictícios
with torch.no_grad():

    y_pred = model(X_test)
    Funcionalidade.graphic(X_test, y_test, y_pred)

    
print(f"MSE = {loss.detach().item()}")
